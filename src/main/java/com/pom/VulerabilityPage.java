package com.pom;

import org.openqa.selenium.By;

public class VulerabilityPage {

	public static By searchbox =By.xpath("//input[@type='search']");
	public static By view =By.xpath("(//p[text()='View'])[1]");
	public static By remediationchart = By.xpath("//div[@id='remediation-dependency-chart']");
	public static By issuecationLable = By.xpath("//a[text()='Issue Details']");
}
