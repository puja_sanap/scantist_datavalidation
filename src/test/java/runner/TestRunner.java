package runner;

import java.io.File;

import org.junit.runner.RunWith;

import com.cucumber.listener.Reporter;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@RunWith(Cucumber.class)
@CucumberOptions(features = {"src/test/resources/features/LoginPage.feature","src/test/resources/features/ProjectPage.feature","src/test/resources/features/VulnerabilityPage.feature"}, glue = "com.test", monochrome = true, plugin = { "pretty", "html:target/cucumber", })
public class TestRunner extends AbstractTestNGCucumberTests{
	public static void writeExtentReport() {
		Reporter.loadXMLConfig(new File("Config/report.xml"));
	}
}

