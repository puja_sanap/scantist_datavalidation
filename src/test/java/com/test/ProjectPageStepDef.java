package com.test;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import com.pom.ProjectPage;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ProjectPageStepDef {
	
	private WebDriver driver;

	public ProjectPageStepDef() {
		this.driver = Hooks.getDriver();
	}
	
	@When("^user is on project page$")
	public void user_is_on_project_page() throws Throwable {
		new WebDriverWait(driver, 6000).until(ExpectedConditions.elementToBeClickable(ProjectPage.projecttab));
		driver.findElement(ProjectPage.projecttab).click();

		if (driver.findElements(By.xpath("//div[@class='enjoyhint_close_btn']")).size() > 0) {

			driver.findElement(By.xpath("//div[@class='enjoyhint_close_btn']")).click();
		} else {
			// System.out.println("*****Element not found1******");
		}
		
	    SoftAssert softAssert = new SoftAssert();
	    String projectPageTitle = driver.findElement(ProjectPage.projectTilte).getText();
	    softAssert.assertEquals(projectPageTitle, "Projects","Page title matching");
	    Actions action = new Actions(driver);
		WebElement card = driver.findElement(By.xpath("(//div[@class='clickable-chart'])[1]"));
		action.moveToElement(card).build().perform();
		Thread.sleep(1000);
		action.click();
	}

	@Then("^Search for project \"([^\"]*)\"$")
	public void search_for_project(String arg1) throws Throwable {
	    driver.findElement(ProjectPage.searchbutton).sendKeys(arg1);
	    driver.findElement(ProjectPage.searchbutton).sendKeys(Keys.ENTER);
	}

	@Then("^Click on vulnerability result$")
	public void click_on_vulnerability_result() throws Throwable {
		
	    driver.findElement(ProjectPage.vulerabilityButton).click();
	}



}
