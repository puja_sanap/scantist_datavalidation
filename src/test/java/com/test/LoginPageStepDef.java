package com.test;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.testng.asserts.SoftAssert;

import com.datadriven.Constant;
import com.datadriven.ExcelUtils;
import com.pom.DashboardPage;
import com.pom.LoginPage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class LoginPageStepDef {

	private WebDriver driver;
	
	public LoginPageStepDef() {
		this.driver = Hooks.getDriver();
	}
	
	@Given("^Launch chrome browser$")
	public void launch_chrome_browser() throws Throwable {
		
	}

	@Given("^user is on homepage$")
	public void user_is_on_homepage() throws Throwable {

	}

	@Then("^user is logged in with valid username and password$")
	public void user_is_logged_in_with_valid_username_and_password() throws Throwable {
		try {
			driver.findElement(LoginPage.Signin).click();
			try {
				ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Sheet1");
				String username = ExcelUtils.getCellData(1, 1);
				String password = ExcelUtils.getCellData(1, 2);

				driver.findElement(LoginPage.Username).clear();
				driver.findElement(LoginPage.Username).sendKeys(username);
				driver.findElement(LoginPage.Password).clear();
				driver.findElement(LoginPage.Password).sendKeys(password);
				driver.findElement(LoginPage.Signin).click();
				driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				ExcelUtils.setCellData("Pass", 1, 3);

			} catch (Exception e) {
				throw e;
			}
		
		
		} catch (Exception e) {
			throw e;
		}
	}

		@Then("^redirect on dashboard page$")
		public void redirect_on_dashboard_page() throws Throwable {
			String dashboardPageTitle="Dashboard";
			String actualDashboardPageTitle = driver.findElement(DashboardPage.dashboardTitle).getText();
			SoftAssert softAssert = new SoftAssert();
			
			softAssert.assertEquals(actualDashboardPageTitle, dashboardPageTitle);
		}

}
