package com.test;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.pom.VulerabilityPage;


public class Utility {
	
	private WebDriver driver;
	public String cveName;
	public Utility() {
		this.driver = Hooks.getDriver();
	}
	
	public void validateData(String Expected_vulnerable_Component,String Expected_patchedComponentVersion,String Expected_detectedIn,String Expected_recommendedFix,String Expected_fixCompatibility) {
	
		driver.findElement(VulerabilityPage.issuecationLable).click();
		driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL, Keys.END);
		if(driver.findElements(By.xpath("//div[@class='issue-title']/span")).size()>0) {
			new WebDriverWait(driver, 2000).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='issue-title']/span")));
			cveName = driver.findElement(By.xpath("//div[@class='issue-title']/span")).getText();
		}
		else {
			new WebDriverWait(driver, 2000).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='attribute-value']")));
			cveName = driver.findElement(By.xpath("//span[@class='attribute-value']")).getText();
		}
		
		Assert.assertEquals(cveName, VulnerabilityPageStepDef.CVE);
		System.out.println("__________________________________");
		System.out.println(VulnerabilityPageStepDef.CVE+" ==>");
		System.out.println("__________________________________");
		
		
		//Vulnerable Component 
		if(driver.findElement(By.xpath("//div/span[text()='Vulnerable Component']")).isDisplayed()) {
			String vulnerable_Component = driver.findElement(By.xpath("(//div[@class='col-md-4']/div[2])[1]")).getText();
			Assert.assertEquals(vulnerable_Component, Expected_vulnerable_Component);
		}
		
		//Patched Component Version 
		if(driver.findElement(By.xpath("//div/span[text()='Patched Component Version']")).isDisplayed()) {
			String patchedComponentVersion = driver.findElement(By.xpath("(//div[@class='col-md-4']/div[2])[2]")).getText();
			Assert.assertEquals(patchedComponentVersion, Expected_patchedComponentVersion);	
		}
		
		//Detected In
		if(driver.findElement(By.xpath("//div/span[text()='Detected In']")).isDisplayed()) {
			String detectedIn = driver.findElement(By.xpath("(//div[@class='col-md-4']/div[2])[3]")).getText();
			Assert.assertEquals(detectedIn, Expected_detectedIn);
		}
		
		//Recommended Fix
		if(driver.findElement(By.xpath("//div/span[text()='Recommended Fix']")).isDisplayed()) {
			String recommendedFix = driver.findElement(By.xpath("(//div[@class='col-md-4']/div[2])[4]")).getText();
			Assert.assertEquals(recommendedFix, Expected_recommendedFix);
		}
		
		//Fix Compatibility
		if(driver.findElement(By.xpath("//div/span[text()='Fix Compatibility']")).isDisplayed()) {
			String fixCompatibility = driver.findElement(By.xpath("(//div[@class='col-md-4']/div[2])[5]")).getText();
			Assert.assertEquals(fixCompatibility, Expected_fixCompatibility);
		}

		//pull request presence
		if(driver.findElements(By.xpath("//button[text()='Create']")).size()>0) {
			System.out.println("\nCreate pull request button is present");
		}else {
			System.out.println("\nCreate pull request button is not present");
		}
		
		//remediation Chart 
		if(driver.findElement(VulerabilityPage.remediationchart).isDisplayed()) {
			System.out.println("\nRemediation chart present");
		}
		
	}

}
