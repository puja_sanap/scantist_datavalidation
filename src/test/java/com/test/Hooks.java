package com.test;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {
	
	public static WebDriver driver;
	@Before("@selenium")
	public void setUp() throws Exception {

		try {
			System.out.println("Chrome Browser Launched");
			System.out.println("________________________________________");
			Path currentRelativePath = Paths.get("");
			String pathToDriver = currentRelativePath.toAbsolutePath().toString() + File.separator + "driver"
					+ File.separator;
			System.setProperty("webdriver.chrome.driver", pathToDriver + "chromedriver.exe");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			options.addArguments("--disable-infobar");

			options.addArguments("--disable-web-security");
			options.addArguments("--no-proxy-server");
			options.addArguments("--no-sandbox");

			Map<String, Object> pref = new HashMap<String, Object>();
			pref.put("credentials_enable_service", false);
			pref.put("profile.password_manager_enabled", false);
			pref.put("profile.default_content_setting_values.notifications", 2);
			options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
			options.setExperimentalOption("useAutomationExtension", false);
			options.setExperimentalOption("prefs", pref);
			
			driver = new ChromeDriver(options);
			driver.manage().window().maximize();
			driver.get("https://staging.scantist.io");
			driver.manage().deleteAllCookies();
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method is used get WebDriver object in whole project
	 * 
	 * @return
	 */

	public static WebDriver getDriver() {
		return driver;
	}

	@After("@selenium1")
	public void closeBrowser() throws Exception {
		driver.quit();
	}


}
