Feature: search project functionality
	
	
  	Scenario Outline: Verify the project page
    When user is on project page
    Then Search for project "<projectname>"
    Then Click on vulnerability result
    
    Examples:
    | projectname      	     |
    | example-java-maven     |
    
   